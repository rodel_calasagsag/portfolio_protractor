/* global browser */
"use strict";

// UTILITIES
var interactor = require("../commons/interactor");
var env = require("../commons/test-env");

// PAGE OBJECTS
var header = require("../page objects/po-header");

var Homepage = function () {
    // CONSTANTS
    var PAGE_TITLE = "4G SIM Only Plans, Mobile Phones, Data Plans | Yatango Mobile";
    var PAGE_URL = env.getBaseURL();

    // ELEMENTS: page sections
    var sectionWhyUs = $('section#vision');

    // SERVICES
    /**
     * Navigates to homepage, then waits for the Yatango logo in primary header 
     * to appear to signal completion of page rendering.
     */
    this.goToMe = function () {
        console.log("\nGoing to homepage");
        browser.get(PAGE_URL);
        browser.ignoreSynchronization = true;
        var ytLogo = header.getElemYtLogo();
        interactor.waitToBeVisible(ytLogo);
    };

    // GETTERS
    this.getExpectedURL = function () {
        return PAGE_URL;
    };
    this.getExpectedTitle = function () {
        return PAGE_TITLE;
    };
};
module.exports = new Homepage();


