"use strict";
var interactor = require('../commons/interactor.js');

var Header = function () {

    // ELEMENTS
    var btnActivate = $('ul.header-right li.hidden-xs a');
    var imgYatangoLogo = $('#header-logo');
    var btnLogin = $('.header-right .btn-login');

    // SERVICES

    /**
     * Clicks the activate button in the top-right portion of the header
     * @returns {undefined}
     */
    this.clickActivateBtn = function () {
        interactor.safeClick(btnActivate);
    };

    this.clickYatangoLogo = function () {
        interactor.safeClick(imgYatangoLogo);
    };

    this.clickLoginBtn = function () {
        var loginModal = require("../page objects/po-login-modal");
        var container = loginModal.getElemLoginModal();

        interactor.safeClick(btnLogin);
        interactor.waitToBeVisible(container);
    }

    // GETTERS
    this.getElemYtLogo = function () {
        return imgYatangoLogo;
    };
};

module.exports = new Header();


