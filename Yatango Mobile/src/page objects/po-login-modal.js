"use strict";
var interactor = require('../commons/interactor.js');

var LoginModal = function () {

    // ELEMENTS
    var divLoginModal = element(by.id('login-modal'));
    var loginModalContainer = $('.modal-dialog');
    var backdrop = $('.modal.fade.ng-isolate-scope.in');

    // SERVICES
    /**
     * Clicks at a point outside of the specified element. Technique depends on
     * browser name. 
     * @param {String} browserEnv Name of the browser
     * @param {Number} xOffset Horizontal offset from the element's top-left corner
     * @param {Number} yOffset Vertical offset from the element's top-left corner
     */
    this.clickOutsideMe = function (browserEnv, xOffset, yOffset) {
        switch (browserEnv) {
            case "chrome":
            case "internet explorer":
                interactor.clickOutsideOfElement(loginModalContainer, xOffset, yOffset);
                break;
            case "firefox":
            default:
                interactor.safeClick(backdrop);
                break;
        }
        interactor.waitToBeAbsent(loginModalContainer);
    };

    // GETTERS
    this.getElemLoginModal = function () {
        return divLoginModal;
    };

};

module.exports = new LoginModal();