"use strict";

var env = require("../commons/test-env");

var ActivatePage = function () {
    // CONSTANTS
    var PAGE_PATH = "activate";
    var PAGE_TITLE = "Activate | Yatango Mobile";
    var PAGE_URL = env.getBaseURL() + PAGE_PATH;

    // SERVICES
        
    // GETTERS
    this.getExpectedURL = function () {
        return PAGE_URL;
    };

    this.getExpectedTitle = function () {
        return PAGE_TITLE;
    };
};

module.exports = new ActivatePage();


