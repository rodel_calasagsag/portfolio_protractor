/* global browser */

"use strict";

// UTILITIES
var interactor = require("../commons/interactor");

// PAGE OBJECTS
var homepg = require("../page objects/po-home");

var NavMenu = function () {

    // ELEMENTS
    var hamburger = $(".menu-links .icon-menu");
    var navMenu = $("nav.shown");
    var pageOverlay = $(".page-overlay");

    // ELEMENTS: Main Menu
    var lnkHome = $('.panel-left>div:nth-child(1) a.menu-title');
    var lnkWhyUs = $('.panel-left>div:nth-child(1) ul#nav li:nth-child(1) a');

    // SERVICES
    /**
     * Clicks the hamburger icon
     */
    this.clickHamburger = function () {
        interactor.safeClick(hamburger);
    };

    /**
     * Closes the Nav menu by clicking at a point outside it -- specifically,
     * the dark-colored page overlay
     */
    this.clickOverlay = function () {
        interactor.safeClick(pageOverlay);
        interactor.waitToBeAbsent(navMenu);
    };

    /**
     * Clicks Main menu > Yatango Mobile
     */
    this.clickMainMenuHome = function () {
        interactor.safeClick(lnkHome);
    };

    /**
     * Clicks Main menu > Why Us
     */
    this.clickWhyUs = function () {
        interactor.safeClick(lnkWhyUs);
        interactor.waitTitleToBe(homepg.getExpectedTitle());
    };

    // GETTERS
    /**
     * Gets the Nav Menu element
     */
    this.getElemNavMenu = function () {
        return navMenu;
    };
};

module.exports = new NavMenu();