/* global expect, browser */

// spec
"use strict";

// PAGE OBJECTS
var header = require("../page objects/po-header");
var homepg = require("../page objects/po-home");
var activatePg = require("../page objects/po-activate");
var loginModal = require("../page objects/po-login-modal");

// ELEMENTS
var loginModalContainer = loginModal.getElemLoginModal();

describe("Header", function () {

    beforeEach(function () {
        homepg.goToMe();
    });

    it("has an 'Activate' button that takes the user to the Activate page"
            + "when clicked", function () {
                header.clickActivateBtn();
                expect(browser.getTitle()).toEqual(activatePg.getExpectedTitle());
                expect(browser.getCurrentUrl()).toEqual(activatePg.getExpectedURL());
            });

    it("has a Login button that brings up the Login modal", function () {
        header.clickLoginBtn();
        expect(loginModalContainer.isDisplayed()).toBeTruthy();
    });

    it("Clicking outside the Login modal will close it", function () {
        var xOffset = -20;
        var yOffset = -20;

        header.clickLoginBtn();
        loginModal.clickOutsideMe(browser.browserName, xOffset, yOffset);
        expect(loginModalContainer.isPresent()).toBe(false);
    });
});