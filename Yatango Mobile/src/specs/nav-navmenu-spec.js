/* global expect, browser */

// spec
"use strict";

// PAGE OBJECTS
var navMenu = require("../page objects/po-navmenu");
var homepg = require("../page objects/po-home");

// SPECIFIC ELEMENTS
var navMenuFinder = navMenu.getElemNavMenu();

// UTILITIES
var interactor = require("../commons/interactor");

describe("Navigation Menu", function () {
    beforeEach(function () {
        homepg.goToMe();
    });

    // testing hamburger icon
    describe("has a hamburger icon", function () {
        it("that opens it up when clicked; "
                + "closes it when reclicked.", function () {
                    // open nav menu
                    console.log("Clicking hamburger to open nav menu");
                    navMenu.clickHamburger();
                    interactor.waitToBePresent(navMenuFinder);
                    expect(navMenuFinder.isDisplayed()).toBeTruthy();

                    // close by clicking hamburger icon
                    console.log("Re-clicking hamburger to close nav menu");
                    navMenu.clickHamburger();
                    interactor.waitToBeAbsent(navMenuFinder);

                    console.log("Expecting nav menu to close");
                    expect(navMenuFinder.isPresent()).toBeFalsy();
                }
        );
    });

    // testing Main Menu (menu 1) elements
    describe("has Menu #1 that contains a link", function () {
        // open nav menu for each test
        beforeEach(function () {
            console.log("Opening nav menu for testing menu 1 elements");
            navMenu.clickHamburger();
            interactor.waitToBeVisible(navMenuFinder);
        });

        it("to the homepage", function () {
            console.log("Clicking 'Home' link");
            navMenu.clickMainMenuHome();

            console.log("Expecting to be taken to homepage");
            expect(browser.getTitle()).toEqual(homepg.getExpectedTitle());
            expect(browser.getCurrentUrl()).toEqual(homepg.getExpectedURL());
        });

        it("to the 'Why Us' section", function () {
            console.log("Clicking 'Why Us' link");

            // take note of the starting URL
            var startedFromHome = true;
            var startingUrl = "";
            browser.getCurrentUrl().then(function (currentUrl) {
                // regex pattern to determine whether "mobile" or "mobile/"
                // is the last portion of a URL. 
                var pattern = /mobile(|\/)$/;
                startedFromHome = pattern.test(currentUrl);
                startingUrl = currentUrl;
            });

            navMenu.clickWhyUs();

            // if not coming from homepage, URL should have '#vision';
            // otherwise, expect the same homepage URL
            console.log("Expecting to be taken to homepage > vision section");
            browser.getCurrentUrl().then(function (landingUrl) {
                if (startedFromHome) {
                    expect(landingUrl).toEqual(startingUrl);
                } else {
                    var expectedUrl = homepg.getExpectedURL().concat("#vision");
                    expect(landingUrl).toEqual(expectedUrl);
                }
            });
        });
    });

    // testing other functionalities
    it("closes when you click outside it", function () {

        // open nav menu
        console.log("Clicking hamburger to open nav menu");
        navMenu.clickHamburger();
        interactor.waitToBePresent(navMenuFinder);

        // close by clicking outside
        console.log("Clicking outside the nav menu");
        navMenu.clickOverlay();

        console.log("Expecting nav menu to close");
        expect(navMenuFinder.isPresent()).toBeFalsy();
    });
});