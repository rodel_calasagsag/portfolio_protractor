/**
 * Manually set the test execution environment here
 */
"use strict";

var TestEnvironment = function () {
    var UAT_DOMAIN = "http://uat.yatango.com.au/mobile/";
    var PROD_DOMAIN = "https://yatango.com.au/mobile/";
    var DEV1_DOMAIN = "http://dev1.yatango.com.au/mobile/";
    var DEV2_DOMAIN = "http://dev2.yatango.com.au/mobile/";


    //manually set this value when needed
    var chosenEnv = 'prod';

    this.getBaseURL = function () {
        switch (chosenEnv) {
            case 'uat':
                return UAT_DOMAIN;
                break;
            case 'dev1':
                return DEV1_DOMAIN;
                break;
            case 'dev2':
                return DEV2_DOMAIN;
                break;
            case 'prod':
                return PROD_DOMAIN;
                break;
            default:
                return PROD_DOMAIN;
                break;
        }
    };
};

module.exports = new TestEnvironment();
