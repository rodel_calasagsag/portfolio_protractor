/* global browser, protractor */
"use strict";

// CONSTANTS
var EXPLICIT_WAIT_TIME = 20000;

var Interactor = function () {
    var EC = protractor.ExpectedConditions;




    // Element Interactions
    /**
     * Waits for the element to be clickable, and then clicks it
     * @param {ElementFinder} element The element to be clicked
     */
    this.safeClick = function (element) {
        var isClickable = EC.elementToBeClickable(element);
        browser.wait(isClickable, EXPLICIT_WAIT_TIME,
                "Element was not clickable after "
                + EXPLICIT_WAIT_TIME + " millis");
        element.click();
    };

    /**
     * Clicks on a point (with respect to the top-left coordinate) outside of the specified element.
     * @param {ElementFinder} element The specified point outside of this element will be clicked
     * @param {Number} xOffset Horizontal offset from the top-left corner of the element
     * @param {Number} yOffset Vertical offset from the top-left corner of the element
     */
    this.clickOutsideOfElement = function (element, xOffset, yOffset) {
        // start the clicking
        browser.actions()
                .mouseMove(element, {x: xOffset, y: yOffset})
                .click()
                .perform();
    };

    // Waits
    /**
     * Waits for the specified <code>element</code> to be visible. Do not use 
     * this function if <code>element</code> is initially not present in the 
     * DOM. Use <code>waitToBePresent()</code> instead.
     * @param {type} element Element expected to become visible
     */
    this.waitToBeVisible = function (element) {
        var isVisible = EC.visibilityOf(element);
        browser.wait(isVisible, EXPLICIT_WAIT_TIME, "Element was not visible after "
                + EXPLICIT_WAIT_TIME + " millis");
    };

    /**
     * Waits for the specified element to be present in the DOM.
     * @param {ElementFinder} element Element expected to become present
     */
    this.waitToBePresent = function (element) {
        var isPresent = EC.presenceOf(element);
        browser.wait(isPresent, EXPLICIT_WAIT_TIME, "Element was not visible after "
                + EXPLICIT_WAIT_TIME + " millis");
    };

    /**
     * Waits for the specified element to be removed from the DOM.
     * @param {ElementFinder} element Element expected to be removed from the DOM
     */
    this.waitToBeAbsent = function (element) {
        var isAbsent = EC.stalenessOf(element);
        browser.wait(isAbsent, EXPLICIT_WAIT_TIME, "Element was still present after "
                + EXPLICIT_WAIT_TIME + " millis");
    };

    /**
     * Waits for the page title to be <code>title</code>
     * @param {String} title The expected page title
     */
    this.waitTitleToBe = function (title) {
        var matchesWithTitle = EC.titleIs(title);
        browser.wait(matchesWithTitle, EXPLICIT_WAIT_TIME,
                "Title did not become " + title + " after "
                + EXPLICIT_WAIT_TIME + " millis");
    };
};

module.exports = new Interactor();