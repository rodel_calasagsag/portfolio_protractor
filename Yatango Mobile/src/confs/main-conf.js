// conf
"use strict";
exports.config = {
    // maximize browser window
    onPrepare: function () {
        browser.driver.manage().window().maximize();
        // set this up to access the browser name in your tests
        browser.getCapabilities().then(function (capabilities) {
            browser.browserName = capabilities.caps_.browserName;
        });
    },
    framework: 'jasmine2',
    specs: ['../specs/nav-navmenu-spec.js'],
//    specs: ['../specs/*-spec.js'],
    seleniumAddress: 'http://localhost:4444/wd/hub',
//    directConnect: true,
    capabilities: {
        'browserName': 'internet explorer'
    }
};